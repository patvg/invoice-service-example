package com.pat.backend.invoice.config;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import io.restassured.RestAssured;

@ActiveProfiles("test")
public class RestAssuredConfiguration {

    @LocalServerPort
    protected int serverPort;

    public void setUpRestAssured() {
        RestAssured.port = serverPort;
    }
}
