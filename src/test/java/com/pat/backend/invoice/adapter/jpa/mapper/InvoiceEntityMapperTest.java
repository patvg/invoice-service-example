package com.pat.backend.invoice.adapter.jpa.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.pat.backend.invoice.adapter.jpa.entity.InvoiceEntity;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.test.util.InvoiceCreateFixture;
import com.pat.backend.invoice.test.util.InvoiceEntityFixture;

public class InvoiceEntityMapperTest {

    @Test
    public void toEntity() throws Exception {
        InvoiceEntityMapper mapper = new InvoiceEntityMapper();
        InvoiceEntity invoiceEntity = new InvoiceEntityFixture().basic().getInvoiceEntity();
        Invoice invoice = mapper.toDomain(invoiceEntity);

        assertThat(invoice).isNotNull();
        assertThat(invoice.getId()).isEqualTo(invoiceEntity.getId());
        assertThat(invoice.getInvoiceNumber()).isEqualTo(invoiceEntity.getInvoiceNumber());
        assertThat(invoice.getPurchaseOrderNumber()).isEqualTo(invoiceEntity.getPurchaseOrderNumber());
        assertThat(invoice.getDueDate()).isEqualTo(invoiceEntity.getDueDate());
        assertThat(invoice.getAmountCents()).isEqualTo(invoiceEntity.getAmountCents());
    }

    @Test
    public void toDomain() throws Exception {
        InvoiceEntityMapper mapper = new InvoiceEntityMapper();
        InvoiceCreate invoiceCreate = new InvoiceCreateFixture().basic().getInvoiceCreate();
        InvoiceEntity invoiceEntity = mapper.toEntity(invoiceCreate);

        assertThat(invoiceEntity).isNotNull();
        assertThat(invoiceEntity.getInvoiceNumber()).isEqualTo(invoiceCreate.getInvoiceNumber());
        assertThat(invoiceEntity.getPurchaseOrderNumber()).isEqualTo(invoiceCreate.getPurchaseOrderNumber());
        assertThat(invoiceEntity.getDueDate()).isEqualTo(invoiceCreate.getDueDate());
        assertThat(invoiceEntity.getAmountCents()).isEqualTo(invoiceCreate.getAmountCents());
    }
}
