package com.pat.backend.invoice.adapter.jpa;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.google.common.collect.Lists;
import com.pat.backend.invoice.adapter.jpa.entity.InvoiceEntity;
import com.pat.backend.invoice.adapter.jpa.jparepository.InvoiceJpaRepository;
import com.pat.backend.invoice.adapter.jpa.mapper.InvoiceEntityMapper;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;
import com.pat.backend.invoice.domain.model.exception.InvoiceExistsException;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceRepositoryImplTest {
    @Mock
    private InvoiceJpaRepository invoiceJpaRepository;
    @Spy
    private InvoiceEntityMapper invoiceEntityMapper;

    @InjectMocks
    private InvoiceRepositoryImpl invoiceRepository;

    @Test
    public void searchInvoice() throws Exception {
        // mock jpa repository call
        PageRequest pageRequest = PageRequest.of(1,1);
        InvoiceEntity invoiceEntity = createInvoiceEntity();
        List<InvoiceEntity> invoices = Lists.newArrayList(invoiceEntity);
        Page<InvoiceEntity> page = new PageImpl<>(invoices, pageRequest, 1);
        when(invoiceJpaRepository.findByInvoiceNumberOrPurchaseOrderNumberOrderByCreatedAtDesc(anyString(), anyString(), any())).thenReturn(page);

        // call search method
        PageMeta pageMeta = new PageMeta();
        pageMeta.setSize(10);
        pageMeta.setNumber(0);
        PageContent<Invoice> pagedInvoices = invoiceRepository.searchInvoice(invoiceEntity.getInvoiceNumber(), invoiceEntity.getPurchaseOrderNumber(), pageMeta);

        assertThat(pagedInvoices.getContent()).hasSize(1);
        assertThat(pagedInvoices.getContent().get(0).getInvoiceNumber()).isEqualTo(invoiceEntity.getInvoiceNumber());
        assertThat(pagedInvoices.getContent().get(0).getPurchaseOrderNumber()).isEqualTo(invoiceEntity.getPurchaseOrderNumber());
    }

    private InvoiceEntity createInvoiceEntity() {
        String invoiceNumber = "ABC";
        String purchaseOrderNumber = "X01";
        Date today = new Date();
        InvoiceEntity invoiceEntity = new InvoiceEntity();
        invoiceEntity.setId(1L);
        invoiceEntity.setCreatedAt(today);
        invoiceEntity.setLastModified(today);
        invoiceEntity.setInvoiceNumber(invoiceNumber);
        invoiceEntity.setPurchaseOrderNumber(purchaseOrderNumber);
        invoiceEntity.setAmountCents(10000);
        invoiceEntity.setDueDate(today);
        return invoiceEntity;
    }

    @Test
    public void createInvoice() throws Exception {
        // mock jpa repository call
        InvoiceEntity invoiceEntity = createInvoiceEntity();
        when(invoiceJpaRepository.save(any(InvoiceEntity.class))).thenReturn(invoiceEntity);

        // call create method
        InvoiceCreate invoiceCreate = createInvoiceCreate();
        Invoice createdInvoice = invoiceRepository.createInvoice(invoiceCreate);
        assertThat(createdInvoice).isNotNull();
        assertThat(createdInvoice.getId()).isEqualTo(1);
        assertThat(createdInvoice.getCreatedAt()).isNotNull();
        assertThat(createdInvoice.getInvoiceNumber()).isEqualTo(invoiceCreate.getInvoiceNumber());
        assertThat(createdInvoice.getPurchaseOrderNumber()).isEqualTo(invoiceCreate.getPurchaseOrderNumber());
        assertThat(createdInvoice.getAmountCents()).isEqualTo(invoiceCreate.getAmountCents());
        assertThat(createdInvoice.getDueDate()).isInSameDayAs(invoiceCreate.getDueDate());

    }

    @Test(expected = InvoiceExistsException.class)
    public void createInvoice_alreadyExists() {
        RuntimeException exception = new RuntimeException("Invoice creation failed", new ConstraintViolationException("Already exists", null, "constraint name"));
        doThrow(exception).when(invoiceJpaRepository).save(any());
        InvoiceCreate invoiceCreate = createInvoiceCreate();
        invoiceRepository.createInvoice(invoiceCreate);
    }

    private InvoiceCreate createInvoiceCreate() {
        String invoiceNumber = "ABC";
        String purchaseOrderNumber = "X01";

        InvoiceCreate invoiceCreate = new InvoiceCreate();
        invoiceCreate.setInvoiceNumber(invoiceNumber);
        invoiceCreate.setPurchaseOrderNumber(purchaseOrderNumber);
        invoiceCreate.setAmountCents(10000);
        invoiceCreate.setDueDate(new Date());
        return invoiceCreate;
    }
}
