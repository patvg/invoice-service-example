package com.pat.backend.invoice.rest.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.pat.backend.invoice.api.vo.InvoiceCreationRequestWS;
import com.pat.backend.invoice.api.vo.InvoiceWS;
import com.pat.backend.invoice.api.vo.PagedInvoiceWS;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;
import com.pat.backend.invoice.test.util.InvoiceCreationRequestWSFixture;
import com.pat.backend.invoice.test.util.InvoiceFixture;

public class InvoiceWSMapperTest {
    @Test
    public void toWS() throws Exception {
        InvoiceWSMapper mapper = new InvoiceWSMapper();
        Invoice invoice = new InvoiceFixture().basic().getInvoice();
        InvoiceWS invoiceWS = mapper.toWS(invoice);

        assertThat(invoiceWS).isNotNull();
        assertThat(invoiceWS.getId()).isEqualTo(String.valueOf(invoice.getId()));
        assertThat(invoiceWS.getAmountCents()).isEqualTo(invoice.getAmountCents());
        assertThat(invoiceWS.getCreatedAt()).isEqualTo(invoice.getCreatedAt());
        assertThat(invoiceWS.getDueDate()).isEqualTo(invoice.getDueDate());
        assertThat(invoiceWS.getInvoiceNumber()).isEqualTo(invoice.getInvoiceNumber());
        assertThat(invoiceWS.getPoNumber()).isEqualTo(invoice.getPurchaseOrderNumber());
    }

    @Test
    public void toDomain() throws Exception {
        InvoiceWSMapper mapper = new InvoiceWSMapper();
        InvoiceCreationRequestWS invoiceCreationRequestWS = new InvoiceCreationRequestWSFixture().basic().getInvoiceCreationRequestWS();
        InvoiceCreate invoiceCreate = mapper.toDomain(invoiceCreationRequestWS);

        assertThat(invoiceCreate).isNotNull();
        assertThat(invoiceCreate.getAmountCents()).isEqualTo(invoiceCreationRequestWS.getAmountCents());
        assertThat(invoiceCreate.getDueDate()).isEqualTo(invoiceCreationRequestWS.getDueDate());
        assertThat(invoiceCreate.getInvoiceNumber()).isEqualTo(invoiceCreationRequestWS.getInvoiceNumber());
        assertThat(invoiceCreate.getPurchaseOrderNumber()).isEqualTo(invoiceCreationRequestWS.getPoNumber());
    }

    @Test
    public void toPagedWS() throws Exception {
        InvoiceWSMapper mapper = new InvoiceWSMapper();
        Invoice invoice = new InvoiceFixture().basic().getInvoice();
        List<Invoice> invoices = Lists.newArrayList(invoice);
        PageContent<Invoice> invoicePageContent = new PageContent<>(invoices, PageMeta.builder().size(10).number(0).totalElements(1).totalPage(1).build());
        PagedInvoiceWS pagedInvoiceWS = mapper.toPagedWS(invoicePageContent);

        assertThat(pagedInvoiceWS).isNotNull();
        assertThat(pagedInvoiceWS.getContent()).hasSize(1);
        assertThat(pagedInvoiceWS.getContent().get(0).getPoNumber()).isEqualTo(invoice.getPurchaseOrderNumber());
        assertThat(pagedInvoiceWS.getContent().get(0).getInvoiceNumber()).isEqualTo(invoice.getInvoiceNumber());
        assertThat(pagedInvoiceWS.getContent().get(0).getDueDate()).isEqualTo(invoice.getDueDate());
        assertThat(pagedInvoiceWS.getContent().get(0).getCreatedAt()).isEqualTo(invoice.getCreatedAt());
        assertThat(pagedInvoiceWS.getContent().get(0).getAmountCents()).isEqualTo(invoice.getAmountCents());
        assertThat(pagedInvoiceWS.getContent().get(0).getId()).isEqualTo(String.valueOf(invoice.getId()));
    }
}
