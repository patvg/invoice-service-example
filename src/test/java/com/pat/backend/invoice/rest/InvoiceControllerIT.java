package com.pat.backend.invoice.rest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pat.backend.Application;
import com.pat.backend.invoice.api.vo.ErrorWS;
import com.pat.backend.invoice.api.vo.InvoiceCreationRequestWS;
import com.pat.backend.invoice.api.vo.InvoiceWS;
import com.pat.backend.invoice.api.vo.PagedInvoiceWS;
import com.pat.backend.invoice.config.InvoiceTestConfiguration;
import com.pat.backend.invoice.config.RestAssuredConfiguration;
import com.pat.backend.invoice.domain.model.exception.InvoiceException;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {Application.class, InvoiceTestConfiguration.class})
public class InvoiceControllerIT extends RestAssuredConfiguration {

    @Before
    public void setUp() throws Exception {
        setUpRestAssured();
    }

    @Test
    public void createInvoice() {
        InvoiceCreationRequestWS request = new InvoiceCreationRequestWS();
        request.setInvoiceNumber("I-1234-create");
        request.setPoNumber("P-123-create4");
        request.setDueDate(new Date());
        request.setAmountCents(10000);
        InvoiceWS response = createInvoice(request);

        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotBlank();
        assertThat(response.getAmountCents()).isEqualTo(request.getAmountCents());
        assertThat(response.getDueDate()).isEqualTo(request.getDueDate());
        assertThat(response.getInvoiceNumber()).isEqualTo(request.getInvoiceNumber());
        assertThat(response.getPoNumber()).isEqualTo(request.getPoNumber());
        assertThat(response.getCreatedAt()).isNotNull();

        // create invoice with the same invoice number results with an error
        ErrorWS errorWS = given()
            .contentType(ContentType.JSON)
            .body(request)
            .post("/invoices")
            .then()
            .statusCode(409)
            .and()
            .extract()
            .response()
            .as(ErrorWS.class);
        assertThat(errorWS).isNotNull();
        assertThat(errorWS.getStatus()).isEqualTo(409);
        assertThat(errorWS.getCode()).isEqualTo(InvoiceException.ErrorCode.INVOICE_ALREADY_EXISTS.toString());
    }

    private InvoiceWS createInvoice(InvoiceCreationRequestWS creationRequestWS) {
        InvoiceWS created = given()
            .contentType(ContentType.JSON)
            .body(creationRequestWS)
            .post("/invoices")
            .then()
            .statusCode(201)
            .and()
            .extract()
            .response()
            .as(InvoiceWS.class);

        return created;
    }

    @Test
    public void searchInvoices() {
        InvoiceCreationRequestWS request = new InvoiceCreationRequestWS();
        request.setInvoiceNumber("I-1234-search");
        request.setPoNumber("P-1234-search");
        request.setDueDate(new Date());
        request.setAmountCents(10000);
        InvoiceWS response = createInvoice(request);

        // result found by invoice number
        PagedInvoiceWS pagedInvoiceWS = given()
            .queryParam("invoice_number", "I-1234-search")
            .get("/invoices")
            .then()
            .statusCode(200)
            .and()
            .extract()
            .response()
            .as(PagedInvoiceWS.class);

        validateSearchResponse(response, pagedInvoiceWS);

        // result found by purchase order number
        pagedInvoiceWS = given()
            .queryParam("purchase_order_number", "P-1234-search")
            .get("/invoices")
            .then()
            .statusCode(200)
            .and()
            .extract()
            .response()
            .as(PagedInvoiceWS.class);
        validateSearchResponse(response, pagedInvoiceWS);

        // result found by invoice number even if purchase order one is not found
        pagedInvoiceWS = given()
            .queryParam("invoice_number", "I-1234-search")
            .queryParam("purchase_order_number", "XXXX")
            .get("/invoices")
            .then()
            .statusCode(200)
            .and()
            .extract()
            .response()
            .as(PagedInvoiceWS.class);
        validateSearchResponse(response, pagedInvoiceWS);

        // no result found
        pagedInvoiceWS = given()
            .queryParam("invoice_number", "XXXX")
            .queryParam("purchase_order_number", "XXXX")
            .get("/invoices")
            .then()
            .statusCode(200)
            .and()
            .extract()
            .response()
            .as(PagedInvoiceWS.class);
        assertThat(pagedInvoiceWS).isNotNull();
        assertThat(pagedInvoiceWS.getContent()).isEmpty();
        assertThat(pagedInvoiceWS.getPage()).isNotNull();
        assertThat(pagedInvoiceWS.getPage().getSize()).isEqualTo(10);
        assertThat(pagedInvoiceWS.getPage().getNumber()).isEqualTo(0);
        assertThat(pagedInvoiceWS.getPage().getTotalElements()).isEqualTo(0);
        assertThat(pagedInvoiceWS.getPage().getTotalPage()).isEqualTo(0);
    }

    private void validateSearchResponse(InvoiceWS response, PagedInvoiceWS pagedInvoiceWS) {
        assertThat(pagedInvoiceWS).isNotNull();
        assertThat(pagedInvoiceWS.getContent()).hasSize(1);
        assertThat(pagedInvoiceWS.getContent().get(0).getId()).isEqualTo(String.valueOf(response.getId()));
        assertThat(pagedInvoiceWS.getContent().get(0).getCreatedAt()).isNotNull();
        assertThat(pagedInvoiceWS.getContent().get(0).getPoNumber()).isEqualTo(response.getPoNumber());
        assertThat(pagedInvoiceWS.getContent().get(0).getInvoiceNumber()).isEqualTo(response.getInvoiceNumber());
        assertThat(pagedInvoiceWS.getContent().get(0).getDueDate()).isInSameDayAs(response.getDueDate());
        assertThat(pagedInvoiceWS.getContent().get(0).getAmountCents()).isEqualTo(response.getAmountCents());

        assertThat(pagedInvoiceWS.getPage()).isNotNull();
        assertThat(pagedInvoiceWS.getPage().getSize()).isEqualTo(10);
        assertThat(pagedInvoiceWS.getPage().getNumber()).isEqualTo(0);
        assertThat(pagedInvoiceWS.getPage().getTotalElements()).isEqualTo(1);
        assertThat(pagedInvoiceWS.getPage().getTotalPage()).isEqualTo(1);
    }

    @Test
    public void searchInvoices_badParameters() {
        ErrorWS errorWS = given()
            .get("/invoices")
            .then()
            .statusCode(400)
            .and()
            .extract()
            .response()
            .as(ErrorWS.class);

        assertThat(errorWS).isNotNull();
        assertThat(errorWS.getStatus()).isEqualTo(400);
        assertThat(errorWS.getCode()).isEqualTo(InvoiceException.ErrorCode.INVALID_INVOICE_SEARCH.toString());

        errorWS = given()
            .queryParam("invoice_number", "XXXX")
            .queryParam("purchase_order_number", "XXXX")
            .queryParam("limit", -1)
            .get("/invoices")
            .then()
            .statusCode(400)
            .and()
            .extract()
            .response()
            .as(ErrorWS.class);

        assertThat(errorWS).isNotNull();
        assertThat(errorWS.getStatus()).isEqualTo(400);
        assertThat(errorWS.getCode()).isEqualTo(InvoiceException.ErrorCode.INVALID_INVOICE_SEARCH.toString());

        given()
            .queryParam("invoice_number", "XXXX")
            .queryParam("purchase_order_number", "XXXX")
            .queryParam("offset", -1)
            .get("/invoices")
            .then()
            .statusCode(400)
            .and()
            .extract()
            .response()
            .as(ErrorWS.class);

        assertThat(errorWS).isNotNull();
        assertThat(errorWS.getStatus()).isEqualTo(400);
        assertThat(errorWS.getCode()).isEqualTo(InvoiceException.ErrorCode.INVALID_INVOICE_SEARCH.toString());
    }
}
