package com.pat.backend.invoice.domain;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceServiceImplTest {
    @Mock
    private InvoiceRepository invoiceRepository;

    @InjectMocks
    private InvoiceServiceImpl invoiceService;

    @Test
    public void searchInvoices() throws Exception {
        String invoiceNumber = "I-1234";
        String purchaseOrderNumber = "P-1234";
        PageMeta pageMeta = PageMeta.builder().size(10).number(0).build();

        PageContent<Invoice> pagedInvoice = invoiceService.searchInvoices(invoiceNumber, purchaseOrderNumber, pageMeta);
        verify(invoiceRepository, times(1)).searchInvoice(anyString(), anyString(), any(PageMeta.class));
    }

    @Test
    public void createInvoice() throws Exception {
        InvoiceCreate invoiceCreate = new InvoiceCreate();
        invoiceService.createInvoice(invoiceCreate);
        verify(invoiceRepository, times(1)).createInvoice(any(InvoiceCreate.class));
    }

}
