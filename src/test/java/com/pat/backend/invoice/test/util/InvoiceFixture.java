package com.pat.backend.invoice.test.util;

import java.util.Date;

import com.pat.backend.invoice.domain.model.Invoice;

public class InvoiceFixture {
    private Invoice invoice;
    public InvoiceFixture() {
        this.invoice = new Invoice();
    }
    
    public Invoice getInvoice() {
        return invoice;
    }
    
    public InvoiceFixture basic() {
        invoice.setId(1L);
        invoice.setInvoiceNumber("I-123ABC");
        invoice.setPurchaseOrderNumber("P-1234ABC");
        invoice.setDueDate(new Date());
        invoice.setAmountCents(10000);
        invoice.setCreatedAt(new Date());
        return this;
    }
}
