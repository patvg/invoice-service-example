package com.pat.backend.invoice.test.util;

import java.util.Date;

import com.pat.backend.invoice.api.vo.InvoiceCreationRequestWS;

public class InvoiceCreationRequestWSFixture {
    private InvoiceCreationRequestWS invoiceCreationRequestWS;

    public InvoiceCreationRequestWSFixture() {
        this.invoiceCreationRequestWS = new InvoiceCreationRequestWS();
    }

    public InvoiceCreationRequestWSFixture basic() {
        invoiceCreationRequestWS.setAmountCents(10000);
        invoiceCreationRequestWS.setDueDate(new Date());
        invoiceCreationRequestWS.setInvoiceNumber("I-1234");
        invoiceCreationRequestWS.setPoNumber("P-1234");
        return this;
    }

    public InvoiceCreationRequestWS getInvoiceCreationRequestWS() {
        return this.invoiceCreationRequestWS;
    }
}
