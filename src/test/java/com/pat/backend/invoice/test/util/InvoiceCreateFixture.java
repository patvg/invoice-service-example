package com.pat.backend.invoice.test.util;

import java.util.Date;

import com.pat.backend.invoice.domain.model.InvoiceCreate;

public class InvoiceCreateFixture {
    private InvoiceCreate invoiceCreate;

    public InvoiceCreateFixture() {
        this.invoiceCreate = new InvoiceCreate();
    }
    
    public InvoiceCreate getInvoiceCreate() {
        return invoiceCreate;
    }
    
    public InvoiceCreateFixture basic() {
        invoiceCreate.setInvoiceNumber("I-123ABC");
        invoiceCreate.setPurchaseOrderNumber("P-1234ABC");
        invoiceCreate.setDueDate(new Date());
        invoiceCreate.setAmountCents(10000);
        return this;
    }
}
