package com.pat.backend.invoice.test.util;

import java.util.Date;

import com.pat.backend.invoice.adapter.jpa.entity.InvoiceEntity;

public class InvoiceEntityFixture {
    private InvoiceEntity invoiceEntity;

    public InvoiceEntityFixture() {
        this.invoiceEntity = new InvoiceEntity();
    }
    
    public InvoiceEntity getInvoiceEntity() {
        return invoiceEntity;
    }
    
    public InvoiceEntityFixture basic() {
        invoiceEntity.setId(1L);
        invoiceEntity.setInvoiceNumber("I-123ABC");
        invoiceEntity.setPurchaseOrderNumber("P-1234ABC");
        invoiceEntity.setDueDate(new Date());
        invoiceEntity.setAmountCents(10000);
        invoiceEntity.setCreatedAt(new Date());
        invoiceEntity.setLastModified(new Date());
        return this;
    }
}
