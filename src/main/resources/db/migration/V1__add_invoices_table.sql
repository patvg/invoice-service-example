CREATE TABLE invoices (
  id SERIAL,
  invoice_number VARCHAR(64) NOT NULL UNIQUE,
  po_number VARCHAR(64) NOT NULL,
  due_date DATE NOT NULL,
  amount_cents BIGINT NOT NULL,
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
  last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()
);
CREATE INDEX invoices_invoice_number on INVOICES (invoice_number);
CREATE INDEX invoices_purchase_order_number on INVOICES (po_number);
