package com.pat.backend.invoice.rest;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pat.backend.invoice.api.InvoicesApi;
import com.pat.backend.invoice.api.vo.InvoiceCreationRequestWS;
import com.pat.backend.invoice.api.vo.InvoiceWS;
import com.pat.backend.invoice.api.vo.PagedInvoiceWS;
import com.pat.backend.invoice.domain.InvoiceService;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;
import com.pat.backend.invoice.domain.model.exception.InvalidInvoiceSearchException;
import com.pat.backend.invoice.rest.mapper.InvoiceWSMapper;
import io.swagger.annotations.ApiParam;

@RestController
public class InvoiceController implements InvoicesApi {
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private InvoiceWSMapper invoiceWSMapper;

    @Override
    public ResponseEntity<InvoiceWS> createInvoice(@Valid @RequestBody InvoiceCreationRequestWS invoiceCreationRequest) {
        Invoice createdInvoice = invoiceService.createInvoice(invoiceWSMapper.toDomain(invoiceCreationRequest));
        return ResponseEntity.status(HttpStatus.CREATED).body(invoiceWSMapper.toWS(createdInvoice));
    }

    @Override
    public ResponseEntity<PagedInvoiceWS> searchInvoice(@ApiParam(value = "search by an invoice number") @RequestParam(value = "invoice_number", required = false) String invoiceNumber, @ApiParam(value = "search by a purchase order number") @RequestParam(value = "purchase_order_number", required = false) String purchaseOrderNumber, @Min(0) @Max(50) @ApiParam(value = "maximum number of records to return", defaultValue = "10") @RequestParam(value = "limit", required = false, defaultValue="10") Integer limit, @Valid @Min(0) @ApiParam(value = "maximum number of records to return", defaultValue = "0") @RequestParam(value = "offset", required = false, defaultValue="0") Integer offset) {
        if (invoiceNumber == null && purchaseOrderNumber == null) {
            throw new InvalidInvoiceSearchException("Invoice number or purchase order number search criteria must be defined.");
        }

        validateParameters(limit, offset);

        PageMeta pageMeta = new PageMeta();
        pageMeta.setSize(limit);
        pageMeta.setNumber(offset);
        PageContent<Invoice> pagedInvoices = invoiceService.searchInvoices(invoiceNumber, purchaseOrderNumber, pageMeta);
        PagedInvoiceWS pagedInvoiceWS = invoiceWSMapper.toPagedWS(pagedInvoices);
        return ResponseEntity.ok(pagedInvoiceWS);
    }

    // Spring MVC doesn't seem to process @Valid for primitives: https://jira.spring.io/browse/SPR-6380
    private void validateParameters(@Min(0) @Max(50) Integer limit, @Valid @Min(0) Integer offset) {
        if (limit < 0 || limit > 50) {
            throw new InvalidInvoiceSearchException("Invalid limit value: " + limit);
        }

        if (offset < 0) {
            throw new InvalidInvoiceSearchException("Invalid offset value: " + offset);
        }
    }
}
