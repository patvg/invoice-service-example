package com.pat.backend.invoice.rest.exception;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.pat.backend.invoice.api.vo.ErrorWS;
import com.pat.backend.invoice.domain.model.exception.InvalidInvoiceSearchException;
import com.pat.backend.invoice.domain.model.exception.InvoiceException;

@Slf4j
@ControllerAdvice
public class AdaptersExceptionHandler {

    @ExceptionHandler(InvoiceException.class)
    public ResponseEntity<ErrorWS> handleException(InvoiceException e, WebRequest request) {
        log.error("Exception thrown by API {}", request.getContextPath(), e);
        ErrorWS error = toErrorWS(e);
        return new ResponseEntity<>(error, null, HttpStatus.valueOf(error.getStatus()));
    }

    @ExceptionHandler(InvalidInvoiceSearchException.class)
    public ResponseEntity<ErrorWS> handleException(InvalidInvoiceSearchException e, WebRequest request) {
        log.error("Exception thrown by API {}", request.getContextPath(), e);
        ErrorWS error = toErrorWS(e);
        return new ResponseEntity<>(error, null, HttpStatus.valueOf(error.getStatus()));
    }

    private HttpStatus getExceptionHttpStatus(InvoiceException exception) {
        switch (exception.getErrorCode()) {
            case INVOICE_ALREADY_EXISTS:
                return HttpStatus.CONFLICT;
            case INVALID_INVOICE_SEARCH:
                return HttpStatus.BAD_REQUEST;
            default:
                return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    private ErrorWS toErrorWS(InvoiceException exception) {
        ErrorWS errorWS = new ErrorWS();
        errorWS.setStatus(getExceptionHttpStatus(exception).value());
        errorWS.setCode(exception.getErrorCode().toString());
        errorWS.setMessage(exception.getMessage());
        return errorWS;
    }
}
