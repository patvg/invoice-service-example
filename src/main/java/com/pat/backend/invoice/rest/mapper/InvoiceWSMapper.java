package com.pat.backend.invoice.rest.mapper;

import java.time.Instant;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.pat.backend.invoice.api.vo.InvoiceCreationRequestWS;
import com.pat.backend.invoice.api.vo.InvoiceWS;
import com.pat.backend.invoice.api.vo.PagedInvoiceWS;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.converter.builtin.ToStringConverter;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
@Slf4j
public class InvoiceWSMapper extends ConfigurableMapper {
    private BoundMapperFacade<Invoice, InvoiceWS> facade = dedicatedMapperFor(Invoice.class, InvoiceWS.class);
    private BoundMapperFacade<InvoiceCreate, InvoiceCreationRequestWS> createFacade = dedicatedMapperFor(InvoiceCreate.class, InvoiceCreationRequestWS.class);
    private BoundMapperFacade<PageContent, PagedInvoiceWS> pagedFacade = dedicatedMapperFor(PageContent.class, PagedInvoiceWS.class);

    @Override
    protected void configure(MapperFactory factory) {
        factory.getConverterFactory().registerConverter(new PassThroughConverter(Instant.class));
        factory.getConverterFactory().registerConverter(new ToStringConverter());

        factory.classMap(Invoice.class, InvoiceWS.class)
            .field("purchaseOrderNumber", "poNumber")
            .byDefault()
            .register();

        factory.classMap(PageContent.class, PagedInvoiceWS.class)
            .byDefault()
            .register();

        factory.classMap(InvoiceCreate.class, InvoiceCreationRequestWS.class)
            .field("purchaseOrderNumber", "poNumber")
            .byDefault()
            .register();
    }

    public InvoiceWS toWS(Invoice Invoice) {
        return facade.map(Invoice);
    }

    public InvoiceCreate toDomain(InvoiceCreationRequestWS creationRequestWS) {
        return createFacade.mapReverse(creationRequestWS);
    }

    public PagedInvoiceWS toPagedWS(PageContent<Invoice> pagedInvoice) {
        return pagedFacade.map(pagedInvoice);
    }
}
