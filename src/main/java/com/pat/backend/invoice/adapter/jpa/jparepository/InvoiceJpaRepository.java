package com.pat.backend.invoice.adapter.jpa.jparepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pat.backend.invoice.adapter.jpa.entity.InvoiceEntity;

@Repository
public interface InvoiceJpaRepository extends JpaRepository<InvoiceEntity, Long> {
    Page<InvoiceEntity> findByInvoiceNumberOrPurchaseOrderNumberOrderByCreatedAtDesc(String invoiceNumber, String purchaseOrderNumber, Pageable pageable);
}
