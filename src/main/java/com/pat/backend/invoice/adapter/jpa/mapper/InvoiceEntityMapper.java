package com.pat.backend.invoice.adapter.jpa.mapper;

import java.time.Instant;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.pat.backend.invoice.adapter.jpa.entity.InvoiceEntity;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
@Slf4j
public class InvoiceEntityMapper extends ConfigurableMapper {
    private BoundMapperFacade<Invoice, InvoiceEntity> facade = dedicatedMapperFor(Invoice.class, InvoiceEntity.class);
    private BoundMapperFacade<InvoiceEntity, InvoiceCreate> facadeCreate = dedicatedMapperFor(InvoiceEntity.class, InvoiceCreate.class);

    @Override
    protected void configure(MapperFactory factory) {
        factory.getConverterFactory().registerConverter(new PassThroughConverter(Instant.class));

        factory.classMap(Invoice.class, InvoiceEntity.class)
            .byDefault()
            .register();

        factory.classMap(InvoiceEntity.class, InvoiceCreate.class)
            .byDefault()
            .register();
    }

    public InvoiceEntity toEntity(InvoiceCreate invoiceCreate) {
        return facadeCreate.mapReverse(invoiceCreate);
    }

    public Invoice toDomain(InvoiceEntity entity) {
        return facade.mapReverse(entity);
    }
}
