package com.pat.backend.invoice.adapter.jpa;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityListeners;

import lombok.extern.slf4j.Slf4j;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.stereotype.Repository;

import com.pat.backend.invoice.adapter.jpa.entity.InvoiceEntity;
import com.pat.backend.invoice.adapter.jpa.jparepository.InvoiceJpaRepository;
import com.pat.backend.invoice.adapter.jpa.mapper.InvoiceEntityMapper;
import com.pat.backend.invoice.domain.InvoiceRepository;
import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;
import com.pat.backend.invoice.domain.model.exception.InvoiceExistsException;

@Repository
@Slf4j
@EntityListeners(AuditingEntityListener.class)
public class InvoiceRepositoryImpl implements InvoiceRepository {
    @Autowired
    private InvoiceJpaRepository invoiceJpaRepository;
    @Autowired
    private InvoiceEntityMapper invoiceEntityMapper;

    @Override
    public PageContent<Invoice> searchInvoice(String invoiceNumber, String purchaseOrderNumber, PageMeta pageMeta) {
        Page<InvoiceEntity> page = invoiceJpaRepository.findByInvoiceNumberOrPurchaseOrderNumberOrderByCreatedAtDesc(invoiceNumber, purchaseOrderNumber, PageRequest.of(pageMeta.getNumber(), pageMeta.getSize()));

        List<Invoice> invoices = page.stream()
            .map(i -> invoiceEntityMapper.toDomain(i))
            .collect(Collectors.toList());

        return new PageContent<>(invoices, new PageMeta(page.getSize(), (int) page.getTotalElements(), page.getTotalPages(), page.getNumber()));
    }

    @Override
    public Invoice createInvoice(InvoiceCreate invoiceCreate) {
        InvoiceEntity invoiceEntity = invoiceEntityMapper.toEntity(invoiceCreate);
        try {
            InvoiceEntity createdInvoice = invoiceJpaRepository.save(invoiceEntity);
            return invoiceEntityMapper.toDomain(createdInvoice);
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                log.warn("The invoice already exists {}", invoiceCreate);
                throw new InvoiceExistsException("The invoice already exists.");
            } else {
                throw e;
            }
        }
    }
}
