package com.pat.backend.invoice.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;

@Component
public class InvoiceServiceImpl implements InvoiceService {
    @Autowired
    private InvoiceRepository invoiceRepository;

    @Override
    public PageContent<Invoice> searchInvoices(String invoiceNumber, String purchaseOrderNumber, PageMeta pageMeta) {
        return invoiceRepository.searchInvoice(invoiceNumber, purchaseOrderNumber, pageMeta);
    }

    @Override
    public Invoice createInvoice(InvoiceCreate invoiceCreate) {
        return invoiceRepository.createInvoice(invoiceCreate);
    }
}
