package com.pat.backend.invoice.domain.model.exception;

import lombok.Getter;

@Getter
public class InvoiceException extends RuntimeException {
    private final ErrorCode errorCode;

    public InvoiceException(String message, ErrorCode errorCode) {
        super(message);
        if (errorCode == null) {
            throw new IllegalArgumentException("The errorCode should not be null");
        }
        this.errorCode = errorCode;
    }

    public enum ErrorCode {
        INVOICE_ALREADY_EXISTS,
        INVALID_INVOICE_SEARCH
    }
}
