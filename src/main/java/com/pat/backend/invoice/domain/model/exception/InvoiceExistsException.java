package com.pat.backend.invoice.domain.model.exception;

public class InvoiceExistsException extends InvoiceException {
    public InvoiceExistsException(String message) {
        super(message, ErrorCode.INVOICE_ALREADY_EXISTS);
    }
}
