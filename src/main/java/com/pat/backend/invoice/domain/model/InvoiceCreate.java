package com.pat.backend.invoice.domain.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
@ToString
public class InvoiceCreate {
    private String invoiceNumber;
    private String purchaseOrderNumber;
    private Date dueDate;
    private int amountCents;
}
