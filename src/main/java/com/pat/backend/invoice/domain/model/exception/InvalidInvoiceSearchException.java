package com.pat.backend.invoice.domain.model.exception;


import lombok.Getter;

@Getter
public class InvalidInvoiceSearchException extends InvoiceException {
    public InvalidInvoiceSearchException(String message) {
        super(message, ErrorCode.INVALID_INVOICE_SEARCH);
    }
}
