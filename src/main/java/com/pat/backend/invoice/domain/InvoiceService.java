package com.pat.backend.invoice.domain;

import com.pat.backend.invoice.domain.model.Invoice;
import com.pat.backend.invoice.domain.model.InvoiceCreate;
import com.pat.backend.invoice.domain.model.PageContent;
import com.pat.backend.invoice.domain.model.PageMeta;

public interface InvoiceService {
    PageContent<Invoice> searchInvoices(String invoiceNumber, String purchaseOrderNumber, PageMeta pageMeta);
    Invoice createInvoice(InvoiceCreate invoiceCreate);
}
