package com.pat.backend.invoice.domain.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor @NoArgsConstructor
@ToString
public class Invoice {
    private long id;
    private String invoiceNumber;
    private String purchaseOrderNumber;
    private Date dueDate;
    private int amountCents;
    private Date createdAt;
}
