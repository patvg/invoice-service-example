package com.pat.backend.invoice.domain.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PageContent<T> {
    private List<T> content;
    private PageMeta page;
}
