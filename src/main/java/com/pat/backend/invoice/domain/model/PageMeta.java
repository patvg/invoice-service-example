package com.pat.backend.invoice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
@Builder
public class PageMeta {
    private int size;
    private int totalElements;
    private int totalPage;
    private int number;
}
