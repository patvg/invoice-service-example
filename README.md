# Invoice REST API
This is a sample project of an invoice REST API.

To run the REST service, simply run this command on your terminal:
```
mvn spring-boot:run
```

The service is also running on Heroku: https://invoice-example.herokuapp.com/invoices.<br>
Note that the first call might take longer as the dyno might be on sleep mode.

If you're using Postman, you can try this collection with pre-configured API requests: https://www.getpostman.com/collections/ad15c95f5ad7ff11243f

## Implementation notes
The REST service has been built with different libraries:
* Spring boot: starter framework to build the REST service
* Flyway to manage database migrations
* Swagger code gen maven plugin to generate the server stubs at compile time

The project assumes that there's a postgresql server on your localhost.<br>
See [postgresql setup on localhost](#setting-local-postgresql).

H2 in-memory database is used for IT tests.


## API Notes
The project is implementing aontract-first approach Using OAS 2.0 specification to define the Invoice API which is hosted on SwaggerHub.<br>
The project downloads the API specification for the defined version at compile time and generates the server stubs which allows to have the API definition and back-end code in sync.

[Full API documentation](https://app.swaggerhub.com/apis-docs/patvong/Invoices/1.0.0)<br>
[Yaml specification](https://app.swaggerhub.com/apiproxy/schema/file/patvong/Invoices/1.0.0/swagger.json?pretty=true)

### GET /invoices
Searches invoices by invoice number or purchase order number.<br><br>
This will return all invoices matching the invoice number or the purchase order number passed by query parameter.<br>
Note: I wasn't sure if the search is meant to be more restrictive only searching by invoice number or by purchase order number only or with both.<br>
My search implementation looks for invoices that matches either of them.

If no invoice number or purchase order number is specified, then I return an error.

### POST /invoices
Creates a new invoice.<br><br>
I assumed that the invoice number is a _public_ unique id and that the `id` in the database is an unique internal generated identifier.<br>
Normally I would have not exposed the internal identifier by the API, but I followed the guidelines with the typical response example.

I also assumed that we can create many invoices to the same purchase order number.

## Local setup
### <a name="setting-local-postgresql"></a>Setting local postgresql
You can create a local postgresql database with a docker image:
```
docker run --name zola-db 
            -e POSTGRES_USER=zola 
            -e POSTGRES_PASSWORD="" 
            -p 5433:5432 
            -e POSTGRES_DB=zola 
            -e POSTGRES_DB_SCHEMA=zola 
            -d postgres
```
*Note that from the above command, you'll have to connect to postgresql using 5433 port.*

### Setting Lombok on your IDE
The project uses lombok to avoid boiler plate code.
To use within your IDE, please follow instruction on [Lombok website](https://projectlombok.org/)
On Intellij, you need to download the [plugin](https://plugins.jetbrains.com/plugin/6317-lombok-plugin) and enable annotation processor on the project.
